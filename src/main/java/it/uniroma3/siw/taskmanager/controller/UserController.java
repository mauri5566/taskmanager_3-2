package it.uniroma3.siw.taskmanager.controller;

import it.uniroma3.siw.taskmanager.controller.session.SessionData;
import it.uniroma3.siw.taskmanager.controller.validation.CredentialsValidator;
import it.uniroma3.siw.taskmanager.controller.validation.UserValidator;
import it.uniroma3.siw.taskmanager.model.Credentials;
import it.uniroma3.siw.taskmanager.model.Project;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.repository.UserRepository;
import it.uniroma3.siw.taskmanager.service.CredentialsService;
import it.uniroma3.siw.taskmanager.service.UserService;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * The UserController handles all interactions involving User data.
 */
@Controller
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserValidator userValidator;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    SessionData sessionData;
    
    @Autowired
    CredentialsService credentialsService;

    @Autowired
    CredentialsValidator credentialsValidator;
    
    @Autowired
    UserService userService;
    /**
     * This method is called when a GET request is sent by the user to URL "/users/user_id".
     * This method prepares and dispatches the User registration view.
     *
     * @param model the Request model
     * @return the name of the target view, that in this case is "register"
     */
    @RequestMapping(value = { "/home" }, method = RequestMethod.GET)
    public String home(Model model) {
        User loggedUser = sessionData.getLoggedUser();
        model.addAttribute("user", loggedUser);
        return "home";
    }

    /**
     * This method is called when a GET request is sent by the user to URL "/users/user_id".
     * This method prepares and dispatches the User registration view.
     *
     * @param model the Request model
     * @return the name of the target view, that in this case is "register"
     */
    @RequestMapping(value = { "/users/me" }, method = RequestMethod.GET)
    public String me(Model model) {
        User loggedUser = sessionData.getLoggedUser();
        Credentials credentials = sessionData.getLoggedCredentials();
        System.out.println(credentials.getPassword());
        model.addAttribute("user", loggedUser);
        model.addAttribute("credentials", credentials);

        return "userProfile";
    }

    /**
     * This method is called when a GET request is sent by the user to URL "/users/user_id".
     * This method prepares and dispatches the User registration view.
     *
     * @param model the Request model
     * @return the name of the target view, that in this case is "register"
     */
    @RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
    public String admin(Model model) {
        User loggedUser = sessionData.getLoggedUser();
        model.addAttribute("user", loggedUser);
        return "admin";
    }
    
    @RequestMapping(value = { "/admin/users" }, method = RequestMethod.GET)
    public String usersList(Model model) {
        User loggedUser = sessionData.getLoggedUser();
        List<Credentials> allCredentials = this.credentialsService.getAllCredentials();
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("credentialsList", allCredentials);
        return "allUsers";
    }
    
    @RequestMapping(value = { "/admin/users/{username}/delete" }, method = RequestMethod.POST)
    public String removeUser(Model model, @PathVariable String username) {
        this.credentialsService.deleteCredentials(username);
        return "redirect:/admin/users";
    }
    
    @RequestMapping(value = { "/users/me/edit" }, method = RequestMethod.GET)
    public String showProjectEdit(Model model, @PathVariable Long id) {
    	User loggedUser = sessionData.getLoggedUser();
    	Credentials credentials = loggedUser.getCredentials();
        model.addAttribute("userForm", loggedUser);
		model.addAttribute("credentialsForm", credentials);

        return "editUser";
    }
    
    @RequestMapping(value = { "/users/me/edit" }, method = RequestMethod.POST)
    public String editProject(@Valid @ModelAttribute("userForm") User user,
            BindingResult userBindingResult,
            @Valid @ModelAttribute("credentialsForm") Credentials credentials,
            BindingResult credentialsBindingResult,
            Model model) {
    	
    	this.userValidator.validate(user, userBindingResult);
        this.credentialsValidator.validate(credentials, credentialsBindingResult);
        User loggedUser = sessionData.getLoggedUser();
        Credentials loggedCredentials = loggedUser.getCredentials();
        if (!(userBindingResult.hasErrors() && credentialsBindingResult.hasErrors())) {
        	loggedUser.setFirstName(user.getFirstName());
            loggedUser.setLastName(user.getLastName());
            loggedCredentials.setUserName(credentials.getUserName());
            loggedCredentials.setPassword(credentials.getPassword());
            this.userService.saveUser(loggedUser);  
            this.credentialsService.saveCredentials(credentials);
            return "redirect:/users/me";
         }
         model.addAttribute("loggedUser", loggedUser);
         return "editUser";
     }
    
    

    

}
