package it.uniroma3.siw.taskmanager.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import it.uniroma3.siw.taskmanager.controller.session.SessionData;
import it.uniroma3.siw.taskmanager.controller.validation.ProjectValidator;
import it.uniroma3.siw.taskmanager.model.Credentials;
import it.uniroma3.siw.taskmanager.model.Project;
import it.uniroma3.siw.taskmanager.model.Task;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.repository.ProjectRepository;
import it.uniroma3.siw.taskmanager.repository.UserRepository;
import it.uniroma3.siw.taskmanager.service.CredentialsService;
import it.uniroma3.siw.taskmanager.service.ProjectService;
import it.uniroma3.siw.taskmanager.service.TaskService;
import it.uniroma3.siw.taskmanager.service.UserService;

@Controller
public class ProjectController {
	
	 public static final String ADMIN_ROLE = "ADMIN";
	 
	@Autowired
	ProjectService projectService;
	
	@Autowired
	ProjectValidator projectValidator;
	
	@Autowired
	SessionData sessionData;
	
	@Autowired
	UserService userService;
	
	@Autowired
	CredentialsService credentialsService;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	TaskService taskService;
	
	@RequestMapping(value = { "/projects/add" }, method = RequestMethod.GET)
    public String showProjectForm(Model model) {
		User loggedUser = sessionData.getLoggedUser();
        model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("projectForm", new Project());

        return "createProject";
    }

    @RequestMapping(value = { "/projects/add" }, method = RequestMethod.POST)
    public String createProject(@Valid @ModelAttribute("projectForm") Project project,
                               BindingResult projectBindingResult, Model model) {
    	User loggedUser = sessionData.getLoggedUser();
        this.projectValidator.validate(project, projectBindingResult);

        // if neither of them had invalid contents, store the User and the Credentials into the DB
        if(!projectBindingResult.hasErrors()) {
            project.setOwner(loggedUser);
        	this.projectService.saveProject(project);
            return "redirect:/projects/" + project.getId();
        }
        model.addAttribute("loggedUser", loggedUser);
        return "createProject";
    }
    
    @RequestMapping(value = { "/projects" }, method = RequestMethod.GET)
    public String myOwnedProjects(Model model) {
        User loggedUser = sessionData.getLoggedUser();
        List<Project> projectsList = projectService.retrieveProjectsOwnedBy(loggedUser);
    	model.addAttribute("loggedUser", loggedUser);
    	model.addAttribute("projectsList", projectsList);
        return "myOwnedProjects";
    }
    
    
    @RequestMapping(value = { "/projects/{projectId}" }, method = RequestMethod.GET)
    public String project(Model model, @PathVariable Long projectId) {
        Project project = projectService.getProject(projectId);
        User loggedUser = sessionData.getLoggedUser();
        if(project==null)
        	return "redirect:/projects";
        List<User> members = userService.getMembers(project);
        List<Task> tasks = project.getTasks();
        if(!project.getOwner().equals(loggedUser) && !members.contains(loggedUser))
        	return "redirect:/projects";
        
        model.addAttribute("tasks", tasks);
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("project", project);
        model.addAttribute("members", members);
        model.addAttribute("credentialsForm", new Credentials());
        
        return "project";
        }
    
    @RequestMapping(value = { "/visibleProjects" }, method = RequestMethod.GET)
    public String sharedWithMe(Model model) {
        User loggedUser = sessionData.getLoggedUser();
        List<Project> projectsList = projectService.retrieveProjectsSharedWith(loggedUser);
    	model.addAttribute("loggedUser", loggedUser);
    	model.addAttribute("projectsList", projectsList);
        return "sharedWithMe";
    }
    
    @RequestMapping(value = { "/projects/{projectId}/delete" }, method = RequestMethod.POST)
    public String removeProject(@PathVariable Long projectId, Model model) {
        this.projectService.deleteProject(projectId);
        return "redirect:/projects";
    }
    
    @RequestMapping(value = { "/projects/{projectId}/share" }, method = RequestMethod.GET)
    public String showProjectShare(Model model, @PathVariable Long projectId) {
    	User loggedUser = sessionData.getLoggedUser();
    	Project project = this.projectService.getProject(projectId);
    	model.addAttribute("project", project);
        model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("credentialsForm", new Credentials());

        return "share";
    }
    
    @RequestMapping(value = { "/projects/{projectId}/share" }, method = RequestMethod.POST)
    public String shareProject(@Valid @ModelAttribute("credentialsForm") Credentials credentials, @PathVariable Long projectId) {
    	
    	Project project = this.projectService.getProject(projectId);
    	User loggedUser = sessionData.getLoggedUser();
    	if(project.getOwner().equals(loggedUser)) {
    		String Username=credentials.getUserName();
    		if(this.credentialsService.getCredentials(Username)!=null) {
				project=this.projectService.shareProjectWithUser(project, 
						this.credentialsService.getCredentials(credentials.getUserName()).getUser());
				project.setOwner(loggedUser);
				this.projectService.saveProject(project);
    		return "redirect:/projects"+projectId;
        	}
    	}
    	return "redirect:/projects"+projectId;
    }
    
    @RequestMapping(value = { "/projects/{id}/edit" }, method = RequestMethod.GET)
    public String showProjectEdit(Model model, @PathVariable Long id) {
		Project project = this.projectService.getProject(id);
    	User loggedUser = sessionData.getLoggedUser();
        model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("projectForm", project);

        return "editProject";
    }
    
    @RequestMapping(value = { "/projects/{id}/edit" }, method = RequestMethod.POST)
    public String editProject(@Valid @ModelAttribute("projectForm") Project projectForm,
                              BindingResult projectBindingResult,
                              Model model,
                              @PathVariable Long id) {
 
        this.projectValidator.validate(projectForm, projectBindingResult);
       
        Project project = projectService.getProject(id);
        Credentials visitor = sessionData.getLoggedCredentials();
        if (project != null) {
            if (!projectBindingResult.hasErrors()) {
                if (visitor.getRole().equals(ADMIN_ROLE) || project.getOwner().getId().equals(visitor.getUser().getId())) {

                    project.setName(projectForm.getName());
                    project.setDescription(projectForm.getDescription());
                    projectService.saveProject(project);
                }
                return "redirect:/projects/" + id;
            }
            model.addAttribute("visitor", visitor);
            return "editProject";
        }
        return "redirect:/projects/" + id;
    }
    
    
    
    
}

