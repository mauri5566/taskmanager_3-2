package it.uniroma3.siw.taskmanager.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.siw.taskmanager.controller.session.SessionData;
import it.uniroma3.siw.taskmanager.controller.validation.TaskValidator;
import it.uniroma3.siw.taskmanager.model.Credentials;
import it.uniroma3.siw.taskmanager.model.Project;
import it.uniroma3.siw.taskmanager.model.Task;
import it.uniroma3.siw.taskmanager.model.User;
import it.uniroma3.siw.taskmanager.repository.ProjectRepository;
import it.uniroma3.siw.taskmanager.repository.TaskRepository;
import it.uniroma3.siw.taskmanager.service.ProjectService;
import it.uniroma3.siw.taskmanager.service.TaskService;

import org.springframework.web.bind.annotation.PathVariable;
@Controller
public class TaskController {
	
	public static final String ADMIN_ROLE = "ADMIN";
	
	@Autowired
	SessionData sessionData;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	TaskValidator taskValidator;
	
	@Autowired
	TaskService taskService;
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	TaskRepository taskRepository;
	
	
	
	@RequestMapping(value = { "/projects/{projectId}/addTask" }, method = RequestMethod.GET)
    public String showTaskForm(@PathVariable Long projectId, Model model) {
		User loggedUser = sessionData.getLoggedUser();
		Project currentProject = this.projectService.getProject(projectId);
        model.addAttribute("currentProject", currentProject);
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("taskForm", new Task());

        return "createTask";
    }
	
	@RequestMapping(value = { "/projects/{projectId}/addTask" }, method = RequestMethod.POST)
    public String createTask(@Valid @ModelAttribute("taskForm") Task taskForm,
                               BindingResult taskBindingResult, @PathVariable Long projectId, Model model) {
    	User loggedUser = sessionData.getLoggedUser();
    	Project project = this.projectService.getProject(projectId);
        this.taskValidator.validate(taskForm, taskBindingResult);
        if(project!=null) {
        	if(!taskBindingResult.hasErrors()) {
        		this.taskService.saveTask(taskForm);
        		project.addTask(taskForm);
        		this.projectService.saveProject(project);
        		return "redirect:/projects/" + projectId;
        	}
        	model.addAttribute("loggedUser", loggedUser);
        	return "createTask";
        }	
        return "redirect:/projects/" + projectId;
	}
	
	@RequestMapping(value = { "/projects/{projectId}/{taskId}/delete" }, method = RequestMethod.POST)
	public String delete(@PathVariable Long projectId, @PathVariable Long taskId, Model model) {	     
		Project project = this.projectService.getProject(projectId);
		Task task = this.taskService.getTask(taskId);
		project.removeTask(task);
		this.taskService.deleteTask(taskId);
		return "redirect:/projects";
	    }
	
	
	@RequestMapping(value = { "/projects/{projectId}/tasks" }, method = RequestMethod.GET)
    public String showTasks(@PathVariable Long projectId, Model model) {
		User loggedUser = sessionData.getLoggedUser();
		Project project = this.projectService.getProject(projectId);
        List<Task> tasks = project.getTasks();
		model.addAttribute("project", project);
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("tasks", tasks);

        return "tasks";
    }
	
	@RequestMapping(value = { "/projects/{projectId}/tasks/{taskId}" }, method = RequestMethod.GET)
    public String showTask(@PathVariable Long projectId, @PathVariable Long taskId, Model model) {
		User loggedUser = sessionData.getLoggedUser();
        Task task = this.taskService.getTask(taskId);
        Project project = this.projectService.getProject(projectId);
		model.addAttribute("project", project);
        model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("task", task);

        return "task";
    }
	
	@RequestMapping(value = { "/projects/{projectId}/tasks/{taskId}/edit" }, method = RequestMethod.GET)
    public String showTaskEdit(@PathVariable Long projectId, @PathVariable Long taskId, Model model) {
		Task task = this.taskService.getTask(taskId);
    	User loggedUser = sessionData.getLoggedUser();
    	Project project = this.projectService.getProject(projectId);
    	model.addAttribute("project", project);
    	model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("taskForm", task);

        return "editTask";
    }
	
	@RequestMapping(value = { "/projects/{projectId}/tasks/{taskId}/edit" }, method = RequestMethod.POST)
    public String editTask(@Valid @ModelAttribute("taskForm") Task taskForm,BindingResult taskBindingResult,
                              @PathVariable Long projectId, @PathVariable Long taskId, Model model) {
 
		this.taskValidator.validate(taskForm, taskBindingResult);
		Project project = this.projectService.getProject(projectId);
        Task task = taskService.getTask(taskId);
        Credentials visitor = sessionData.getLoggedCredentials();
        if (task != null) {
            if (!taskBindingResult.hasErrors()) {
                if (visitor.getRole().equals(ADMIN_ROLE) || project.getOwner().getId().equals(visitor.getUser().getId())) {

                    task.setName(taskForm.getName());
                    task.setDescription(taskForm.getDescription());
                    if(taskForm.isCompleted())
                    	task.setCompleted(true);
                    task.setCompleted(false);
                    taskService.saveTask(task);
                    projectService.saveProject(project);
                }
                return "redirect:/projects";
            }
            model.addAttribute("visitor", visitor);
            return "editTask";
        }
        return "redirect:/projects";
    }
	
	@RequestMapping(value = { "/projects/{projectId}/tasks/{taskId}/setUser" }, method = RequestMethod.GET)
    public String showUserSelect(@PathVariable Long projectId, @PathVariable Long taskId, Model model) {
		Project project = this.projectService.getProject(projectId);
		List<User> users = project.getMembers();
		Task task = this.taskService.getTask(taskId);
		
		model.addAttribute("users", users);
		model.addAttribute("project", project);
		model.addAttribute("task", task);
        return "setUser";
    }
	
	
	
}