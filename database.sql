PGDMP         -                x           taskmanager    12.0    12.0     i           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            j           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            k           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            l           1262    450686    taskmanager    DATABASE     i   CREATE DATABASE taskmanager WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
    DROP DATABASE taskmanager;
                postgres    false            �            1259    527800    credentials    TABLE     S  CREATE TABLE public.credentials (
    id bigint NOT NULL,
    creation_timestamp timestamp without time zone NOT NULL,
    last_update_timestamp timestamp without time zone NOT NULL,
    password character varying(100) NOT NULL,
    role character varying(10) NOT NULL,
    user_name character varying(100) NOT NULL,
    user_id bigint
);
    DROP TABLE public.credentials;
       public         heap    postgres    false            �            1259    527798    hibernate_sequence    SEQUENCE     {   CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.hibernate_sequence;
       public          postgres    false            �            1259    527805    project    TABLE     �   CREATE TABLE public.project (
    id bigint NOT NULL,
    description character varying(255),
    name character varying(100) NOT NULL,
    owner_id bigint
);
    DROP TABLE public.project;
       public         heap    postgres    false            �            1259    527810    project_members    TABLE     q   CREATE TABLE public.project_members (
    visible_projects_id bigint NOT NULL,
    members_id bigint NOT NULL
);
 #   DROP TABLE public.project_members;
       public         heap    postgres    false            �            1259    527813    task    TABLE     ;  CREATE TABLE public.task (
    id bigint NOT NULL,
    completed boolean NOT NULL,
    creation_timestamp timestamp without time zone NOT NULL,
    description character varying(255),
    last_update_timestamp timestamp without time zone NOT NULL,
    name character varying(100) NOT NULL,
    project_id bigint
);
    DROP TABLE public.task;
       public         heap    postgres    false            �            1259    526993 
   task_users    TABLE     j   CREATE TABLE public.task_users (
    task_id bigint NOT NULL,
    users_identificatore bigint NOT NULL
);
    DROP TABLE public.task_users;
       public         heap    postgres    false            �            1259    527818    users    TABLE     -  CREATE TABLE public.users (
    id bigint NOT NULL,
    creation_timestamp timestamp without time zone NOT NULL,
    first_name character varying(100) NOT NULL,
    last_name character varying(100) NOT NULL,
    last_update_timestamp timestamp without time zone NOT NULL,
    credentials_id bigint
);
    DROP TABLE public.users;
       public         heap    postgres    false            b          0    527800    credentials 
   TABLE DATA           x   COPY public.credentials (id, creation_timestamp, last_update_timestamp, password, role, user_name, user_id) FROM stdin;
    public          postgres    false    204   �$       c          0    527805    project 
   TABLE DATA           B   COPY public.project (id, description, name, owner_id) FROM stdin;
    public          postgres    false    205   �%       d          0    527810    project_members 
   TABLE DATA           J   COPY public.project_members (visible_projects_id, members_id) FROM stdin;
    public          postgres    false    206   �%       e          0    527813    task 
   TABLE DATA           w   COPY public.task (id, completed, creation_timestamp, description, last_update_timestamp, name, project_id) FROM stdin;
    public          postgres    false    207   �%       `          0    526993 
   task_users 
   TABLE DATA           C   COPY public.task_users (task_id, users_identificatore) FROM stdin;
    public          postgres    false    202   .&       f          0    527818    users 
   TABLE DATA           u   COPY public.users (id, creation_timestamp, first_name, last_name, last_update_timestamp, credentials_id) FROM stdin;
    public          postgres    false    208   K&       m           0    0    hibernate_sequence    SEQUENCE SET     @   SELECT pg_catalog.setval('public.hibernate_sequence', 4, true);
          public          postgres    false    203            �           2606    527804    credentials credentials_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.credentials
    ADD CONSTRAINT credentials_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.credentials DROP CONSTRAINT credentials_pkey;
       public            postgres    false    204            �           2606    527809    project project_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.project DROP CONSTRAINT project_pkey;
       public            postgres    false    205            �           2606    527817    task task_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.task DROP CONSTRAINT task_pkey;
       public            postgres    false    207            �           2606    527004 '   task_users uk_fartl3prgo7ngqtkwcwh0phon 
   CONSTRAINT     r   ALTER TABLE ONLY public.task_users
    ADD CONSTRAINT uk_fartl3prgo7ngqtkwcwh0phon UNIQUE (users_identificatore);
 Q   ALTER TABLE ONLY public.task_users DROP CONSTRAINT uk_fartl3prgo7ngqtkwcwh0phon;
       public            postgres    false    202            �           2606    527824 (   credentials uk_iruybducdoxd2f0vh3t8g6x5y 
   CONSTRAINT     h   ALTER TABLE ONLY public.credentials
    ADD CONSTRAINT uk_iruybducdoxd2f0vh3t8g6x5y UNIQUE (user_name);
 R   ALTER TABLE ONLY public.credentials DROP CONSTRAINT uk_iruybducdoxd2f0vh3t8g6x5y;
       public            postgres    false    204            �           2606    527822    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    208            �           2606    527835 +   project_members fk6qakef2mfjhaoaqep3b9qneea    FK CONSTRAINT     �   ALTER TABLE ONLY public.project_members
    ADD CONSTRAINT fk6qakef2mfjhaoaqep3b9qneea FOREIGN KEY (members_id) REFERENCES public.users(id);
 U   ALTER TABLE ONLY public.project_members DROP CONSTRAINT fk6qakef2mfjhaoaqep3b9qneea;
       public          postgres    false    206    208    3035            �           2606    527850 !   users fk732k8fvdmf8q8msul077ck3a5    FK CONSTRAINT     �   ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk732k8fvdmf8q8msul077ck3a5 FOREIGN KEY (credentials_id) REFERENCES public.credentials(id);
 K   ALTER TABLE ONLY public.users DROP CONSTRAINT fk732k8fvdmf8q8msul077ck3a5;
       public          postgres    false    204    208    3027            �           2606    527830 #   project fk7tetln4r9qig7tp05lsdqe8xo    FK CONSTRAINT     �   ALTER TABLE ONLY public.project
    ADD CONSTRAINT fk7tetln4r9qig7tp05lsdqe8xo FOREIGN KEY (owner_id) REFERENCES public.users(id);
 M   ALTER TABLE ONLY public.project DROP CONSTRAINT fk7tetln4r9qig7tp05lsdqe8xo;
       public          postgres    false    208    3035    205            �           2606    527825 '   credentials fkcbcgksvnqvqxrrc4dwv3qys65    FK CONSTRAINT     �   ALTER TABLE ONLY public.credentials
    ADD CONSTRAINT fkcbcgksvnqvqxrrc4dwv3qys65 FOREIGN KEY (user_id) REFERENCES public.users(id);
 Q   ALTER TABLE ONLY public.credentials DROP CONSTRAINT fkcbcgksvnqvqxrrc4dwv3qys65;
       public          postgres    false    204    3035    208            �           2606    527845     task fkk8qrwowg31kx7hp93sru1pdqa    FK CONSTRAINT     �   ALTER TABLE ONLY public.task
    ADD CONSTRAINT fkk8qrwowg31kx7hp93sru1pdqa FOREIGN KEY (project_id) REFERENCES public.project(id);
 J   ALTER TABLE ONLY public.task DROP CONSTRAINT fkk8qrwowg31kx7hp93sru1pdqa;
       public          postgres    false    205    207    3031            �           2606    527840 +   project_members fkkkowdb1552cnnmu8apvugooo0    FK CONSTRAINT     �   ALTER TABLE ONLY public.project_members
    ADD CONSTRAINT fkkkowdb1552cnnmu8apvugooo0 FOREIGN KEY (visible_projects_id) REFERENCES public.project(id);
 U   ALTER TABLE ONLY public.project_members DROP CONSTRAINT fkkkowdb1552cnnmu8apvugooo0;
       public          postgres    false    3031    206    205            b   z   x�3�4202�50�5�P0��21�2�г4�!lȩb��bh�b�����liZn�ZZ�o�Z�j�����X�haR�jX���隚��V�������XZ�ojjf�i����� g"I      c   )   x�3�LI-N.ʬ���+(�OO-)�����M�s��b���� C      d      x������ � �      e   @   x�3�,�4202�50�5�P0��21�21ճ0�LI-N.ʬ���K-I,�ƥ,/?"o����� ;F$      `      x������ � �      f   :   x�3�4202�50�5�P0��21�2�г41��M,-ʬ���t*���ĩ,Ə+F��� `	     